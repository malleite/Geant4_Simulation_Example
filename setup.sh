#!/bin/bash

#Setup GEANT4 path
export GEANT4=/home/leite/Work/Cosmic/Geant4-10.7.1
alias setup_geant4='source $GEANT4/bin/geant4.sh'

#Setup VTK path
export VTK=/home/leite/Work/Cosmic/VTK-9.0.1

export PYTHONPATH=$GEANT4/lib64/python3.6:$VTK/lib64

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GEANT4/lib64:$VTK/lib64