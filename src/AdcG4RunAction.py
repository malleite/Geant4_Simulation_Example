"""
AdcG4RunAction

    This class provides methods to perform actions 
    before/end of a run. 

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

import Geant4
Geant4 = Geant4


class AdcG4RunAction(Geant4.G4UserRunAction):
    """
        AdcG4 Run Action
    """
    def BeginOfRunAction(self, run):
        print ("===> INFO: Beggining  of Run")
        print ("===> INFO: Run summary : id= %d, #events= %d" \
            % (run.GetRunID(), run.GetNumberOfEventToBeProcessed()) )
    
        ga = Geant4.gRunManager.GetUserPrimaryGeneratorAction()

        print ("===> INFO: Primary Name:", ga.GetParticleGun().GetParticleByName())
        print ("===> INFO: Primary PDG:", ga.GetParticleGun().GetParticleDefinition().GetPDGEncoding() )
        print ("===> INFO: Primary Energy [GeV]:", ga.GetParticleGun().GetParticleEnergy()/1000.)
        print ("===> INFO: Primary Momentum :", ga.GetParticleGun().GetParticleMomentumDirection().x,\
                                      ga.GetParticleGun().GetParticleMomentumDirection().y,\
                                      ga.GetParticleGun().GetParticleMomentumDirection().z )
        print ("===> INFO: Primary Number:", ga.GetParticleGun().GetNumberOfParticles() )
        print (" ")

    def EndOfRunAction(self, run):
        """
            Execute only once at the end of a run. Will get the run manager "run",
            which is a singleton.
        """

        Geant4.gRunManager.AbortRun()

        print ("===> INFO: End of Run")

