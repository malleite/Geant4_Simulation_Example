"""
AdcG4EventAction

    This class will build the geometry based on gdml file and provide helper
    methods to  interact or get information from this geometry. 

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

import array

import datetime
import time
import Geant4
Geant4 = Geant4

#import AdcG4HitCollection

class AdcG4EventAction(Geant4.G4UserEventAction):
    """
        Start/End of event specific actions
    """
    def setHitCollectionName(self,adcG4HitCollection=None):
        """
            Complains if I do it at initialization
        """
        self.AdcG4HitCollection = adcG4HitCollection
        
        
    def setTree(self,adcTree=None):
        """
            Complains if I do it at initialization
            Get the tree pointer for filling
        """
        self.adcTree = adcTree
            
    def BeginOfEventAction (self, event):
            
        for value in self.AdcG4HitCollection.values() :
            value.clear()
            
      
    def EndOfEventAction(self, event):
        """
            Based on the content of the hit collection, fill other collections and/or do the necessary calculations
            to account for the interesting quantities.  
            Just a dummy method for now
        """
        chunk = 10000.0
        ev = event.GetEventID()
        if (ev == 0) :
            self.initTime = datetime.datetime.now()
            self.nevents = Geant4.gRunManager.GetCurrentRun().GetNumberOfEventToBeProcessed()
        else :
            if (ev%chunk == 0) :
                x = datetime.datetime.now() - self.initTime
                x = chunk/datetime.timedelta.total_seconds(x)
                eta = (self.nevents - ev) / x 
                seta = time.strftime('%Hh:%Mm:%Ss', time.gmtime(eta))

                print ("===> INFO: Processed ", ev, " events (", float(x), " events/s) - ETA: ",seta )
                
                self.initTime= datetime.datetime.now()

        #Need this information to save the primary
        ga = Geant4.gRunManager.GetUserPrimaryGeneratorAction()
       
        self.adcTree.primary_PdgId[0] = ga.GetParticleGun().GetParticleDefinition().GetPDGEncoding()
        self.adcTree.primary_Energy[0] = float(ga.GetParticleGun().GetParticleEnergy())
        self.adcTree.primary_x[0] = float(ga.GetParticleGun().GetParticleMomentumDirection().x)
        self.adcTree.primary_y[0] = float(ga.GetParticleGun().GetParticleMomentumDirection().y)
        self.adcTree.primary_z[0] = float(ga.GetParticleGun().GetParticleMomentumDirection().z)
        self.adcTree.primary_Number[0] = int(ga.GetParticleGun().GetNumberOfParticles())
        
        self.adcTree.runNumber[0] = 1
        self.adcTree.eventNumber[0] = int(event.GetEventID())
        
        k = 0
        n = 0
        nSum = 0
        
        for key,value in self.AdcG4HitCollection.items() :
            #print "Collection Size:",key, len(value.dEdx), len(value.particleId)
            for i in range(value.n) :
                self.adcTree.adc_volumeId.push_back(k)
                self.adcTree.adc_dEdx.push_back(value.dEdx[i])
                self.adcTree.adc_PdgId.push_back(value.particleId[i])
                self.adcTree.adc_kineticEnergy.push_back(value.kineticEnergy[i])
                n = n + 1
              
            self.adcTree.adc_Sum_volumeId.push_back(k)   
            self.adcTree.adc_Sum_dEdx.push_back(sum(value.dEdx))
            nSum = nSum + 1
            k = k + 1
                

        self.adcTree.adc_n[0] = n
        self.adcTree.adc_Sum_n[0] = nSum
            
        self.adcTree.tree.Fill() 
           
        #Clear the vectors
        self.adcTree.adc_volumeId.clear()    
        self.adcTree.adc_dEdx.clear()
        self.adcTree.adc_PdgId.clear()
        self.adcTree.adc_kineticEnergy.clear()
        
        self.adcTree.adc_Sum_volumeId.clear()
        self.adcTree.adc_Sum_dEdx.clear()
        

        '''
        print "End of Event ", self.adcTree.eventNumber[0]
        print "Entrien in tree", self.adcTree.tree.GetEntries()
        
        print "Name of primary:", ga.GetParticleGun().GetParticleByName()
        print "PDG  of primary:", self.adcTree.primary_PdgId[0]
        print "Energy of primary [GeV]:", self.adcTree.primary_Energy[0]/1000.
        print "Momentum of primary:", self.adcTree.primary_x[0], self.adcTree.primary_y[0], self.adcTree.primary_z[0]
        print "Number of primaries:", self.adcTree.primary_Number[0]
        '''

        