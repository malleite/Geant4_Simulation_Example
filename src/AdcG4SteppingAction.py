"""
AdcG4SteppingAction

    User stepping action class

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

import Geant4
Geant4 = Geant4

class AdcG4SteppingAction(Geant4.G4UserSteppingAction):
    """
        User stepping Action
    """

    def UserSteppingAction(self, step):
        Geant4.G4VSensitiveDetector("vdetector")
    
        dedxTot = step.GetTotalEnergyDeposit()
        dedxNonIon = step.GetNonIonizingEnergyDeposit()
        print "*** dE/dx in current step: Tot=", dedxTot, "Non Ionizing=",dedxNonIon
        #preStepPoint= step.GetPreStepPoint()
        track= step.GetTrack()
        touchable= track.GetTouchable()
        print "*** vid= ", touchable.GetReplicaNumber()