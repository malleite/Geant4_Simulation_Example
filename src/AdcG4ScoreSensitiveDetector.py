"""
AdcG4ScoreSensitiveDetector

    This class is responsible to provide methods for a given sensitive detector
    defined somewhere else.
    Each sensitive volume will have one instance of this class, gathering
    information about the energy deposit, particle types in track etc.
    There are two possibilityes to use this information:

    1) Create a hit collection and fill it here. At the end of an
    event (run manager will call AdcEventAction) the hit collection
    will be then used to fill other collection (like a root tree or vtk array)

    2) Fill directly here the other container. This is not as good as 1)
    as it makes a less modular design : this routine needs to be edited in
    case the other collections change.

    TODO: change to the 1)

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

import Geant4
import string
#import vtk
import AdcG4HitCollection

Geant4 = Geant4

class AdcG4ScoreSensitiveDetector(Geant4.G4VSensitiveDetector):
    """"
    Score a sensitive Volume

    """

    def __init__(self,sdName="defaultSD", adcG4HitCollection = None):
        Geant4.G4VSensitiveDetector.__init__(self, Geant4.G4String(sdName))

        self.sdName = sdName
        self.adcG4HitCollection = adcG4HitCollection

        self.particleDict = {}

    def EndOfEvent(self, event):
        print ("========= Event ", event.GetEventID() )

    def Initialize(self,axx):
        print ("OoOoOoOo ========= Event " )

    def ProcessHits(self, step, rohist):

        preStepPoint= step.GetPreStepPoint()
        #if(preStepPoint.GetCharge() == 0): #FIXME: Is this correct ?????
        #    return

        track= step.GetTrack()
        sn = track.GetCurrentStepNumber()
        particleKineticEnergy = track.GetKineticEnergy()

        ##Check this, new G4 version does not have GetParticleDefinition()
        particle = track.GetParticleDefinition()
        particlePdgId = particle.GetPDGEncoding()
        particleName = particle.GetParticleName()

        pos = track.GetPosition()
        deltapos = step.GetDeltaPosition()
        touchable= track.GetTouchable()
        voxel_id= touchable.GetReplicaNumber()
        dedxTot = step.GetTotalEnergyDeposit()

        #Get Energy in GeV
        dedxTot = dedxTot/1000.

        #print particleName, particlePdgId, particleKineticEnergy
        self.particleDict[particlePdgId] = particleName.__str__()
        #print self.particleDict
        if particleName.__str__() == "neutron" :
            print ('Neutron !, Ek=', particleKineticEnergy, " MeV -- " , particlePdgId )

        self.adcG4HitCollection[self.sdName].n = self.adcG4HitCollection[self.sdName].n + 1
        self.adcG4HitCollection[self.sdName].dEdx.append(dedxTot)
        self.adcG4HitCollection[self.sdName].particleId.append(particlePdgId)
        self.adcG4HitCollection[self.sdName].position.append((pos.x, pos.y, pos.z))
        self.adcG4HitCollection[self.sdName].kineticEnergy.append(particleKineticEnergy)


        #Access evento info from run Manager
        #curEvent = Geant4.gRunManager.GetCurrentEvent().GetEventID()

        """
        if curEvent == self.ev :
            self.eSum = self.eSum + dedxTot/1000.
            #print particle.GetPDGEncoding()
            self.adcG4HitCollection[self.sdName].dEdx.append(dedxTot)
            self.adcG4HitCollection[self.sdName].particleId.append(particlePdgId)
        else :
            print "*****", str(self.sdName),curEvent, self.eSum
            #self.histo[str(self.sdName)]['dEdx']['all'].Fill(self.eSum)
            self.eSum = 0
            self.ev = curEvent
        """


        """
        #Fill the histograms
        pn = str(particleName)
        if pn in self.particleList :
            pass
        else :
            pn = "other"
        self.histo[str(self.sdName)]['dEdx'][pn].Fill(dedxTot)
        self.histo[str(self.sdName)]['track'][pn].Fill(pos.z,pos.y,dedxTot)

        #For all Particles
        #self.histo[str(self.sdName)]['dEdx']['all'].Fill(dedxTot)
        #self.histo[str(self.sdName)]['track']['all'].Fill(pos.z,pos.y,dedxTot)

        """
        """
        #Fill the array that holds the position of the track

        self.vtkTrack[0].InsertNextTupleValue((pos.x,pos.y,pos.z))
        self.vtkTrack[1].InsertNextValue(dedxTot)
        self.vtkTrack[2].InsertNextCell(1)
        self.vtkTrack[2].InsertCellPoint(self.n)
        #self.vtkTrack[2].InsertCellPoint(1)
        self.n = self.n + 1




        #Need to normalize tho the world coordinates that are 2000 for now
        #the Detector is not at the center, displace it also
        x = int(pos.x/20. + 49)
        y = int(pos.y/20. + 49)
        z = int(pos.z/40. + 49)
        ijk = x*10000 + y*100 + z
        #print x,y,z
        pv = self.vtkTrack.GetValue(ijk)
        self.vtkTrack.SetValue(ijk,dedxTot+pv)
        """
