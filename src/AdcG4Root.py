"""
AdcG4Root

    Definition and methods to deal with ROOT objects (trees and histos)

//////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
//////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

import ROOT
import array

ROOT=ROOT

class AdcG4Root ():
    
    def __init__ (self):
        """
            Prepare the ROOT Tree for writing 
            TODO: Create a meta tree to hold volumeId
            TODO: Create a detail level option to include only sum in the tree
            
        """
        print ("HHHHHH")
        self.tree = ROOT.TTree("AdcG4","Adc Geant4")
        
         
        self.runNumber = array.array('i',[0])
        self.eventNumber = array.array('i',[0])       
        
        self.primary_PdgId = array.array('i',[0])
        self.primary_Energy = array.array('f',[0.])
        self.primary_x = array.array('f',[0.])
        self.primary_y = array.array('f',[0.])
        self.primary_z = array.array('f',[0.])
        self.primary_Number = array.array('i',[0])

        self.adc_n = array.array('i',[0])
        self.adc_dEdx = ROOT.std.vector(float)()
        self.adc_PdgId = ROOT.std.vector(int)()
        self.adc_volumeId = ROOT.std.vector(int)()
        self.adc_kineticEnergy =  ROOT.std.vector(float)()
        
        self.adc_Sum_n = array.array('i',[0])
        self.adc_Sum_dEdx = ROOT.std.vector(float)()
        #self.adc_Sum_PdgId = ROOT.std.vector(float)()
        self.adc_Sum_volumeId = ROOT.std.vector(float)()
        
        
        n = 1

        self.tree.Branch('runNumber',  self.runNumber,  'runNumber/I')
        self.tree.Branch('eventNumber',self.eventNumber,'eventNumber/I')
        
        #Primary information
        self.tree.Branch('primary_PdgId', self.primary_PdgId, 'primary_PdgId/I')
        self.tree.Branch('primary_Energy',self.primary_Energy,'primary_Energy/F')
        self.tree.Branch('primary_x',     self.primary_x,     'primary_x/F')
        self.tree.Branch('primary_y',     self.primary_y,     'primary_y/F')
        self.tree.Branch('primary_z',     self.primary_z,     'primary_z/F')
        self.tree.Branch('primary_Number',self.primary_Number,'primary_Number/I') 
        
        #Track information
        self.tree.Branch('adc_n',        self.adc_n,  'acd_n/I')
        self.tree.Branch('adc_PdgId',    self.adc_PdgId)
        self.tree.Branch('adc_dEdx',     self.adc_dEdx)
        self.tree.Branch('adc_volumeId', self.adc_volumeId)
        self.tree.Branch('adc_kineticEnergy', self.adc_kineticEnergy)
        
        #Energy sum per volume
        self.tree.Branch('adc_Sum_n',        self.adc_Sum_n, 'acd_Sum_n/I')
        self.tree.Branch('adc_Sum_dEdx',     self.adc_Sum_dEdx)
        self.tree.Branch('adc_Sum_volumeId', self.adc_Sum_volumeId)
        #self.tree.Branch('zdc_Sum_PdgId',    self.zdc_PdgId)
               
    def saveTree(self):
        
        outFileName = '/tmp/AdcG4.root'
        print ("===> INFO: Saving ROOT file", outFileName)
        
        self.outFile = ROOT.TFile(outFileName,"RECREATE")
    
        #self.outFile.Write()
        self.tree.Write()
        self.outFile.Close()
        