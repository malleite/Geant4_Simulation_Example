"""
AdcG4Main

    No Classes here, just call everything needed to run ...

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"


import AdcG4Run
import ROOT

ROOT=ROOT

gfile1 = "Geometry/Setup01/AdcG4Geometry.gdml"
gfile1 = "Geometry/Setup01/ZdcG4Geometry.gdml"
#gfile1 = "Geometry/Example/test.gdml"

x = AdcG4Run.AdcG4Run(gdmlFileName = gfile1)


x.AdcInitVisualization()

x.AdcDC.getLogicalVolumes()
x.AdcDC.Colorize()
print (x.AdcDC.logicalVolumes)


x.setSensitiveDetector()


#Position here is in cm, while GDML is in mm !!!
#Energy in GeV

for particle in ["proton"] :

    x.bulet["MomentumDir"] = [0., 10., -1.]
    x.bulet['Position'] = [0.,0.,100.]
    x.bulet['Particle'] = particle
    x.bulet['Energy'] = 0.025
    x.bulet['Number'] = 1

    x.AdcRevolver(n=5000)

x.AdcTree.saveTree()

del(x)

print ("This is the end my friend.....")

