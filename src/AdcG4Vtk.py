"""
AdcG4Vtk

    Visualization routines based on vtk. There are many ways to define vtk 
    objects to hold the data. 
    The most important are energy and particle type (per position). 
    Still needs to check VTK documentation to understand what is the best 
    method to use. 
    
    This class also implements a render method, to display the information 
    from the vtk objects inside while running the Geant code.

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$

! ! ! ! 
WARNING : THIS IS BASED ON OLD VTK RESLEASE< AND IS MODIFYED SO AS JUST TO GET 
RUNNING. MUST CHECK VTK DOCS AS MANY THINGS HAVE CHANGED
! ! ! !
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

import vtk

class AdcG4Vtk():
    """
        Visualization class based on the Visualization Toolkit (VTK) for ADC Geant4 simulation. 
    """
    
    def __init__(self) :
        self.pcoords = vtk.vtkFloatArray()
        self.pcoords.SetNumberOfComponents(3)
      
        self.dedx = vtk.vtkFloatArray()
        self.dedx.SetName("dedx")
   
        self.cellArray = vtk.vtkCellArray()

        #Test with uniform grid
        
        self.myArray = vtk.vtkFloatArray()
        self.myArray.SetName("dEdx")
        self.myArray.SetNumberOfComponents(1)
        self.myArray.SetNumberOfTuples(100*100*100)

        self.render()
       
    def render(self):
        """
            Renderization for the objects defined in the __init__ method
        """
        
        """
        #Original test
        points = vtk.vtkPoints()
        points.SetData(self.pcoords)
        
        polydata = vtk.vtkPolyData()
        polydata.SetPoints(points)
        polydata.GetPointData().SetScalars(self.dedx)
        polydata.SetVerts(self.cellArray)
       
        
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInput(polydata)
        mapper.SetScalarRange(0, 100)

        # Create an actor.
        self.actor = vtk.vtkActor()
        self.actor.SetMapper(mapper)
        ww = self.actor.GetProperty()

        # Create the rendering objects.
        self.ren = vtk.vtkRenderer()
        self.ren.AddActor(self.actor)

        self.renWin = vtk.vtkRenderWindow()
        self.renWin.AddRenderer(self.ren)

        self.iren = vtk.vtkRenderWindowInteractor()
        self.iren.SetRenderWindow(self.renWin)
        
        writer = vtk.vtkPolyDataWriter()
        #writer.SetInput(polydata)
        writer.SetInput(polydata)
        writer.SetFileName("xxx.vtk")
        writer.Write()



        self.iren.Initialize()
        self.renWin.Render()
        self.iren.Start()

        """

        xid = vtk.vtkImageData()
        xid.SetOrigin(0,0,0)
        xid.SetSpacing(1,1,1)
        xid.SetDimensions(100,100,100)
        #xid.SetScalarType(vtk.VTK_FLOAT)
        xid.AllocateScalars(vtk.VTK_FLOAT,3)
        xid.GetPointData().SetScalars(self.myArray)
        array = xid.GetPointData().GetArray('dEdx')
        print (array.GetRange())

        renwin = vtk.vtkRenderWindow()
        renderer = vtk.vtkRenderer()
        renwin.AddRenderer(renderer)

        
        mapper = vtk.vtkDataSetMapper()
        mapper.SetInputData(xid)
        #mapper.SetInput(xid)

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        renderer.AddViewProp(actor)
        renderer.ResetCamera()

        
        mapper.ScalarVisibilityOn()

        self.myArray.GetRange()
        mapper.SetScalarRange(0,100)

        
        #raw_input("...")
        iren = vtk.vtkRenderWindowInteractor()
        renwin.SetInteractor(iren)
   
        writer = vtk.vtkDataSetWriter()
        #writer.SetInput(xid)
        writer.SetInputData(xid)
        print ("****************************************")
        print ("**** VTK file will be saved on /tmp *** ")
        print ("****************************************")
        writer.SetFileName("/tmp/xxx.vtk")
        writer.Write()
        
        #iren.Initialize()
        #renwin.Render()
        #iren.Start()
        