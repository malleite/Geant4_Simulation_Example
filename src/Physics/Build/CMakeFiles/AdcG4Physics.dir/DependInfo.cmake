# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/leite/Work/Cosmic/Simulation/Geant4_Simulation_Example/src/Physics/AdcG4PhysicsList.cc" "/home/leite/Work/Cosmic/Simulation/Geant4_Simulation_Example/src/Physics/Build/CMakeFiles/AdcG4Physics.dir/AdcG4PhysicsList.cc.o"
  "/home/leite/Work/Cosmic/Simulation/Geant4_Simulation_Example/src/Physics/AdcG4Physics_wrapper.cc" "/home/leite/Work/Cosmic/Simulation/Geant4_Simulation_Example/src/Physics/Build/CMakeFiles/AdcG4Physics.dir/AdcG4Physics_wrapper.cc.o"
  "/home/leite/Work/Cosmic/Simulation/Geant4_Simulation_Example/src/Physics/GammaPhysics.cc" "/home/leite/Work/Cosmic/Simulation/Geant4_Simulation_Example/src/Physics/Build/CMakeFiles/AdcG4Physics.dir/GammaPhysics.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "AdcG4Physics_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/python3.6m"
  "/home/leite/Work/Cosmic/Geant4-10.7.1/bin/../include/Geant4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
