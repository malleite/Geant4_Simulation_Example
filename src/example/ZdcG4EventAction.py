"""
ZdcG4EventAction

    This class will build the geometry based on gdml file and provide helper
    methods to  interact or get information from this geometry. 

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

import array

import datetime
import time
import Geant4
Geant4 = Geant4

#import ZdcG4HitCollection

class ZdcG4EventAction(Geant4.G4UserEventAction):
    """
        Start/End of event specific actions
    """
    def setHitCollectionName(self,zdcG4HitCollection=None):
        """
            Complains if I do it at initialization
        """
        self.ZdcG4HitCollection = zdcG4HitCollection
        
        
    def setTree(self,zdcTree=None):
        """
            Complains if I do it at initialization
            Get the tree pointer for filling
        """
        self.zdcTree = zdcTree
            
    def BeginOfEventAction (self, event):
            
        for value in self.ZdcG4HitCollection.values() :
            value.clear()
            
      
    def EndOfEventAction(self, event):
        """
            Based on the content of the hit collection, fill other collections and/or do the necessary calculations
            to account for the interesting quantities.  
            Just a dummy method for now
        """
        chunk = 10.0
        ev = event.GetEventID()
        if (ev == 0) :
            self.initTime = datetime.datetime.now()
            self.nevents = Geant4.gRunManager.GetCurrentRun().GetNumberOfEventToBeProcessed()
        else :
            if (ev%chunk == 0) :
                x = datetime.datetime.now() - self.initTime
                x = chunk/datetime.timedelta.total_seconds(x)
                eta = (self.nevents - ev) / x 
                seta = time.strftime('%Hh:%Mm:%Ss', time.gmtime(eta))

                print "===> INFO: Processed ", ev, " events (", float(x), " events/s) - ETA: ",seta
                
                self.initTime= datetime.datetime.now()

            
        #Need this information to save the primary
        ga = Geant4.gRunManager.GetUserPrimaryGeneratorAction()
       
        self.zdcTree.primary_PdgId[0] = ga.GetParticleGun().GetParticleDefinition().GetPDGEncoding()
        self.zdcTree.primary_Energy[0] = float(ga.GetParticleGun().GetParticleEnergy())
        self.zdcTree.primary_x[0] = float(ga.GetParticleGun().GetParticleMomentumDirection().x)
        self.zdcTree.primary_y[0] = float(ga.GetParticleGun().GetParticleMomentumDirection().y)
        self.zdcTree.primary_z[0] = float(ga.GetParticleGun().GetParticleMomentumDirection().z)
        self.zdcTree.primary_Number[0] = int(ga.GetParticleGun().GetNumberOfParticles())
        
        self.zdcTree.runNumber[0] = 1
        self.zdcTree.eventNumber[0] = int(event.GetEventID())
        
        k = 0
        n = 0
        nSum = 0
        
        for key,value in self.ZdcG4HitCollection.items() :
            #print "Collection Size:",key, len(value.dEdx), len(value.particleId)
            for i in range(value.n) :
                self.zdcTree.zdc_volumeId.push_back(k)
                self.zdcTree.zdc_dEdx.push_back(value.dEdx[i])
                self.zdcTree.zdc_PdgId.push_back(value.particleId[i])
                n = n + 1
              
            self.zdcTree.zdc_Sum_volumeId.push_back(k)   
            self.zdcTree.zdc_Sum_dEdx.push_back(sum(value.dEdx))
            nSum = nSum + 1
            k = k + 1
                

        self.zdcTree.zdc_n[0] = n
        self.zdcTree.zdc_Sum_n[0] = nSum
            
        self.zdcTree.tree.Fill() 
           
        #Clear the vectors
        self.zdcTree.zdc_volumeId.clear()    
        self.zdcTree.zdc_dEdx.clear()
        self.zdcTree.zdc_PdgId.clear()
        
        self.zdcTree.zdc_Sum_volumeId.clear()
        self.zdcTree.zdc_Sum_dEdx.clear()

        '''
        print "End of Event ", self.zdcTree.eventNumber[0]
        print "Entrien in tree", self.zdcTree.tree.GetEntries()
        
        print "Name of primary:", ga.GetParticleGun().GetParticleByName()
        print "PDG  of primary:", self.zdcTree.primary_PdgId[0]
        print "Energy of primary [GeV]:", self.zdcTree.primary_Energy[0]/1000.
        print "Momentum of primary:", self.zdcTree.primary_x[0], self.zdcTree.primary_y[0], self.zdcTree.primary_z[0]
        print "Number of primaries:", self.zdcTree.primary_Number[0]
        '''

        