"""
ZdcG4Main

    No Classes here, just call everything needed to run ...

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"


import ZdcG4Run 
import ROOT

ROOT=ROOT

gfile1 = "ZdcGeometry/ZdcG4Geometry.gdml"


x = ZdcG4Run.ZdcG4Run(gdmlFileName = gfile1)


x.ZdcInitVisualization()

x.ZdcDC.getLogicalVolumes()
x.ZdcDC.Colorize()
print x.ZdcDC.logicalVolumes


x.setSensitiveDetector()
   

#Position here is in cm, while GDML is in mm !!!

for particle in ["gamma","neutron"] :

    x.bulet["MomentumDir"] = [0., 0., -1.]
    x.bulet['Position'] = [0.,0.,100.]
    x.bulet['Particle'] = particle
    x.bulet['Energy'] = 1
    x.bulet['Number'] = 1

    x.ZdcRevolver(n=400)
    
x.ZdcTree.saveTree()

