"""
ZdcG4HitCollection

    Collection that will define the hit structure to hold all pertinet information
    Use numpy as it is very convenient and fast. 
    
    TODO: Take a look on Fluka usrdump for a hint ...
    
    per event it will hold :
    
    de/dx (float) 
    particle (pdgId)
    volume identification
    
    The usual framework will output the particle and volume as strings. This may be 
    not convenient to include in a root file, for example. We will need to map this 
    to a numeric structure; I can borrow ATLAS concept of detector ID for example (but of 
    course in this case event the "non detector elements" need to have an ID.
    This will be handled by another class.
    
    The way Geant uses the Hit collection is one collection per SD. 
    A map of ZdcG4HitCollection will be used with detector as a key 
    (unless I find a way to exchange a HitCollection between the SD scorer
    and the event handler ...). May this can be done as a member of SD.
    In this way, I would call it myScore.zdcHitcollection. The advantage is to avoid passing the
    HitCollection to the SDScorer (even tought this is passed at the init of the function).
    Needs some profiling if optimzation is necessary.
    

///////////////////////////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

import numpy as np
import collections

class ZdcG4HitCollection ():
    
    def __init__ (self):
        self.n = 0
        self.dEdx = collections.deque()
        self.particleId = collections.deque()
        self.position = collections.deque()
        
    def clear(self):
        self.n = 0
        self.dEdx.clear()
        self.particleId.clear()
        self.position.clear()
        
    
        
        