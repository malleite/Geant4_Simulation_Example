"""
ZdcG4DetectorConstruction

    This class will build the geometry based on gdml file and provide helper 
    methods to  interact or get information from this geometry. 

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

import Geant4
Geant4 = Geant4

class ZdcG4DetectorConstruction(Geant4.G4VUserDetectorConstruction):
    '''
    Creates the Detector Construction Methods  in G4. 
    This can use GDML, or something else. This generalization 
    can be done latter. Provides methods to extract the logical volumes and 
    to change color of the volumes
    '''

    def __init__(self,gdmlFileName = None):
        Geant4.G4VUserDetectorConstruction.__init__(self)
        self.world= None
        self.gdml_parser= Geant4.G4GDMLParser()

        #TODO: Check the existence of this file
        self.gdmlFileName = gdmlFileName
        
        self.logicalVolumes = {}


    def __del__(self):
        pass
    

    def Construct(self):
        self.gdml_parser.Read(self.gdmlFileName)
        self.world= self.gdml_parser.GetWorldVolume()

        return self.world
    
 
    def getLogicalVolumes(self):
    
        """ 
        Get the names of the logical volumes. The point here is that one may have more than one 
        volume with the same name (if you construct the volumes in a loop for example). Then you 
        need to make each key different adding some more information. We need to loop until there are no more daughter
        volumes
        """
        
        w = self.world.GetLogicalVolume()
        nd = w.GetNoDaughters()
        for i in range(nd) :
            p = w.GetDaughter(i)
            print nd, i, str(p.GetName())
            key = str(p.GetName())+"_"+str(i)
            pp = p.GetLogicalVolume()
            ndd = pp.GetNoDaughters()
            print "-->", ndd
            if ndd > 0 :
                neta = {}
                for j in range(ndd) :
                    px = pp.GetDaughter(j).GetLogicalVolume()
                    print i, j, key, str(px.GetName())
                    neta[str(px.GetName())+"_"+str(j)] = px
                self.logicalVolumes[key] = [pp,neta]
            else :
                self.logicalVolumes[key] = [pp,{}]
            
        
    def Colorize(self):
        """
            This seems to be the only way to put colors in the geometry
        """
        r = 0.0
        g = 0.0
        b = 0.0
        
        i = 0
        if len (self.logicalVolumes) == 0 :
            print "---> ERROR : Call getLogicalVolumes first."
            print "---> ERROR : No colors will be set."
        else :
            for key,value in self.logicalVolumes.items() :
                
                if i<10 : 
                    r = r + 0.1
                    g = 0
                    b = 0
                if (i>=10) & (i<20) :
                    r = 0
                    g = g + 0.1
                    b = 0
                if (i >=20) & (i <30):
                    r = 0
                    g = 0
                    b = b + 0.1
                if (i >= 30) :
                    i = 0
                i = i + 1
                    
                va = Geant4.G4VisAttributes( Geant4.G4Color(r,g,b))
                value[0].SetVisAttributes(va)
                print key,"  ",i,"---  ",r,g,b

