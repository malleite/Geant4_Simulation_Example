"""
ZdcG4Run

    This defines a top class to provide all Geant4 instantiation and 
    other initializations. It will also initialize the visualization 
    (vtk and Geant based) and the ROOT trees and histogram, 
    instantiating the respective classes.
    
    TODO: Move the ROOT initialization to ZdcG4Root file/class

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

#Generic imports
import string
import struct

#Geant4 imports
import Geant4
import g4py.ParticleGun 

#ROOT imports
import ROOT

#Specific imports
import ZdcG4Physics
import ZdcG4DetectorConstruction
import ZdcG4RunAction
import ZdcG4EventAction
import ZdcG4ScoreSensitiveDetector
import ZdcG4HitCollection
import ZdcG4Root
import ZdcG4Vtk



ROOT=ROOT
Geant4=Geant4
ZdcG4Physics=ZdcG4Physics


class ZdcG4Run():
    """
        Main class to run the simulation of the ZDC Geometry using Geant4.  
        The strategy is set to direct to several packages the Geant4 Outuput
         
        - Use Geant4 for simulation and data store
        - Save data as root file
        - Control histograms are generated/filled while the program is running 
        - Visualization is done using VTK/Paraview toolkit
    """


    def __init__(self, gdmlFileName = None):
        
        self.histo = {}
        self.zdcG4HitCollection = {}
        self.myScore = {}
        
        
        """
            Initializes the Geant4 Services 
        """
        
        #TODO: Test if file exists
        self.gdmlFileName = gdmlFileName
        
        #Prepare a different seed each time it runs
        rndNum = self.getRandomSeed("I")
        print "======> INFO: Initializing Random Number Generator with seed = ",rndNum
        self.rand_engine= Geant4.Ranlux64Engine()
        Geant4.HepRandom.setTheEngine(self.rand_engine)
        Geant4.HepRandom.setTheSeed(rndNum)
        
        
        #Geometry
        print "======> INFO: Initializing Geometry using ", self.gdmlFileName
        self.ZdcDC= ZdcG4DetectorConstruction.ZdcG4DetectorConstruction(self.gdmlFileName)
        Geant4.gRunManager.SetUserInitialization(self.ZdcDC)
        
        
        #Physics List; For other process, it may need to be done at external C++ code
        print "======> INFO: Initializing Physics List " 
        self.ZdcPL = ZdcG4Physics.ZdcG4PhysicsList()
        Geant4.gRunManager.SetUserInitialization(self.ZdcPL)
       

        print "======> INFO: Initializing Run Action " 
        self.ZdcRA= ZdcG4RunAction.ZdcG4RunAction()
        Geant4.gRunManager.SetUserAction(self.ZdcRA)
        
        print "======> INFO: Initializing Event User Action " 
        self.ZdcEA = ZdcG4EventAction.ZdcG4EventAction()
        self.ZdcEA.setHitCollectionName(self.zdcG4HitCollection)
        Geant4.gRunManager.SetUserAction(self.ZdcEA)
        
        print "======> INFO: Initializing the particle gun "
        self.ZdcPg= g4py.ParticleGun.Construct()
        
        #call the VTK wrapper
        print "======> INFO: Initializing VTK for visualization"
        self.vtk = ZdcG4Vtk.ZdcG4Vtk()
        
        #ROOT tree for output
        print "======> INFO: Initializing ROOT Services"
        self.ZdcTree = ZdcG4Root.ZdcG4Root()
        self.ZdcEA.setTree(self.ZdcTree)
        
        Geant4.gRunManager.Initialize()
        self.ZdcDC.Colorize()
        
        #Save the RunManager
        self.RunManager = Geant4.gRunManager.GetRunManager()
        
           
        #the  default case for Particle Gun
      
        self.bulet = {}
        self.bulet["Number"] = 1
        self.bulet["Particle"] = "gamma"
        self.bulet["Energy"] = 1
        self.bulet["MomentumDir"] = [0., 0., -1.]
        self.bulet["Position"] = [0., 0., 250.]
 
        

        
    def getRandomSeed(self,tipo = "I"):
        """
            Return a random seed of size "tipo" (number of bytes)
        """
        validTypes = ["h","H","i","I","l","L","q","Q"]
        if tipo not in validTypes :
            print "======> ERROR: type must be one of these: ", validTypes
            return -1
        
        nb = struct.Struct(tipo).size
        print "======> INFO: Getting Seed from /dev/random :"
        f = open("/dev/random","rb")
        rndStr = f.read(nb)
        rndNum= struct.unpack(tipo, rndStr)[0]
        
        return rndNum
        
    def setSensitiveDetector (self):
        """
            Create Sensite Detctor for all volumes by default
        """
        for lv in self.ZdcDC.logicalVolumes.keys() :
            sdName = lv + "_SD"
            self.zdcG4HitCollection[sdName] = ZdcG4HitCollection.ZdcG4HitCollection()
            self.myScore[lv] = ZdcG4ScoreSensitiveDetector.ZdcG4ScoreSensitiveDetector(sdName,self.zdcG4HitCollection)
            self.ZdcDC.logicalVolumes[lv][0].SetSensitiveDetector(self.myScore[lv])


    def ZdcRevolver(self,n = 10):
        """
            Setup a particle gun
        """
     
        
        px = self.bulet["MomentumDir"][0]
        py = self.bulet["MomentumDir"][1]
        pz = self.bulet["MomentumDir"][2]
        
        x = self.bulet["Position"][0]
        y = self.bulet["Position"][1]
        z = self.bulet["Position"][2]
        
        self.ZdcPg.SetNumberOfParticles(self.bulet["Number"])
        self.ZdcPg.SetParticleByName(self.bulet["Particle"])
        self.ZdcPg.SetParticleEnergy(self.bulet["Energy"]*Geant4.GeV)
        self.ZdcPg.SetParticleMomentumDirection(Geant4.G4ThreeVector(px,py,pz))
        self.ZdcPg.SetParticlePosition(Geant4.G4ThreeVector(x,y,z)*Geant4.cm)
        
        
        self.RunManager.BeamOn(n)
       

    def ZdcInitVisualization(self):
        """
            visualization of the gun firing ...
        """


        Geant4.gApplyUICommand("/vis/open OGLIX")
        Geant4.gApplyUICommand("/vis/scene/create")
        Geant4.gApplyUICommand("/vis/scene/add/volume")
        Geant4.gApplyUICommand("/vis/scene/add/axes")
        Geant4.gApplyUICommand("/vis/scene/add/hits")
        Geant4.gApplyUICommand("/vis/scene/add/eventId")
        Geant4.gApplyUICommand("/vis/sceneHandler/attach")
        Geant4.gApplyUICommand("/vis/viewer/set/viewpointThetaPhi 60. -30.")
        Geant4.gApplyUICommand("/vis/viewer/set/style solid")
        Geant4.gApplyUICommand("/vis/viewer/set/style wired")
        Geant4.gApplyUICommand("/vis/viewer/set/viewpointThetaPhi 70. 30.")
        
        Geant4.gApplyUICommand("/tracking/storeTrajectory 1")
        Geant4.gApplyUICommand("/vis/scene/add/trajectories")
        
        Geant4.gApplyUICommand("/vis/scene/endOfEventAction accumulate")
        
        Geant4.gApplyUICommand("/vis/viewer/zoomTo 1.2")
        
   
