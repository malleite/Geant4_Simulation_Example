//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: RE01PhysicsList.cc,v 1.2 2006-06-29 17:44:07 gunter Exp $
// GEANT4 tag $Name: not supported by cvs2svn $
//

#include "Include/AdcG4PhysicsList.hh"

#include "Include/AdcG4Physics_Lepton.hh"
#include "Include/AdcG4Physics_Hadron.hh"
#include "Include/AdcG4Physics_Boson.hh"
#include "Include/AdcG4Physics_Decay.hh"
#include "Include/AdcG4Physics_Ion.hh"

AdcG4PhysicsList::AdcG4PhysicsList():  G4VModularPhysicsList()
{
  // default cut value  (1.0mm) 
  defaultCutValue = 0.01 * mm;
  SetVerboseLevel(1);

  // Particle decays
  RegisterPhysics( new AdcG4Physics_Decay("decay"));

  // Bosons (gamma + geantinos)
  RegisterPhysics( new AdcG4Physics_Boson("boson"));

  // Leptons
  RegisterPhysics( new AdcG4Physics_Lepton("lepton"));

  // Hadron Physics
  RegisterPhysics( new AdcG4Physics_Hadron("hadron"));

  // Ion Physics
  RegisterPhysics( new AdcG4Physics_Ion("ion"));
}

AdcG4PhysicsList::~AdcG4PhysicsList()
{;}

void AdcG4PhysicsList::SetCuts()
{
  // Use default cut values gamma and e processes
  SetCutsWithDefault();   
  // Set the secondary production cut lower than 990. eV
  // Very important for high precision of lowenergy processes at low energies

   G4double lowLimit = 25. * eV;
   G4double highLimit = 50 * MeV;
   G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(lowLimit, highLimit);

}



