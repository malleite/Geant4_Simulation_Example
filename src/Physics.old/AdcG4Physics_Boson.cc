//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: RE01BosonPhysics.cc,v 1.2 2006-06-29 17:43:33 gunter Exp $
// GEANT4 tag $Name: not supported by cvs2svn $
//
//

#include "Include/AdcG4Physics_Boson.hh"

#include "G4ProcessManager.hh"
#include "G4GammaConversion.hh"
#include "G4ComptonScattering.hh"
#include "G4PhotoElectricEffect.hh"

#include "G4PenelopeComptonModel.hh"

#include "G4ChargedGeantino.hh"
#include "G4Geantino.hh"
#include "G4Gamma.hh"


AdcG4Physics_Boson::AdcG4Physics_Boson(const G4String& name)
                     :  G4VPhysicsConstructor(name) {}


AdcG4Physics_Boson::~AdcG4Physics_Boson() {}


void AdcG4Physics_Boson::ConstructParticle()
{
  // pseudo-particles
  G4Geantino::GeantinoDefinition();
  G4ChargedGeantino::ChargedGeantinoDefinition();
  // gamma
  G4Gamma::GammaDefinition();  
}


void AdcG4Physics_Boson::ConstructProcess()
{
   // Add e+e- pair creation, Compton scattering and photo-electric effect
   // to gamma

   G4ProcessManager* pManager = G4Gamma::Gamma()->GetProcessManager();
   pManager->AddDiscreteProcess(new G4GammaConversion());
   //pManager->AddDiscreteProcess(new G4ComptonScattering());
   pManager->AddDiscreteProcess(new G4PhotoElectricEffect());

   G4ComptonScattering* theComptonScattering = new G4ComptonScattering();
   theComptonScattering->SetModel(new G4PenelopeComptonModel());
   pManager->AddDiscreteProcess(theComptonScattering);
}


