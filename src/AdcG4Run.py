"""
adcG4Run

    This defines a top class to provide all Geant4 instantiation and
    other initializations. It will also initialize the visualization
    (vtk and Geant based) and the ROOT trees and histogram,
    instantiating the respective classes.

    TODO: Move the ROOT initialization to AdcG4Root file/class

///////////////////////////////////////////////////////////////////////////////
$Author: leite $
$Revision:$
$HeadURL:$

$LastChangedDate:$
$LastChangedBy:$

$Id:$
///////////////////////////////////////////////////////////////////////////////
"""

__version__ = "$Revision:$"

#Generic imports
import string
import struct

#Geant4 imports
import Geant4
#import g4py.ParticleGun
import g4pytest.ParticleGun

#ROOT imports
import ROOT

#Specific imports
import AdcG4Physics
import AdcG4DetectorConstruction
import AdcG4RunAction
import AdcG4EventAction
import AdcG4ScoreSensitiveDetector
import AdcG4HitCollection
import AdcG4Root
import AdcG4Vtk



ROOT=ROOT
Geant4=Geant4
AdcG4Physics=AdcG4Physics


class AdcG4Run():
    """
        Main class to run the simulation of the ADC Test Geometry using Geant4.
        The strategy is set to direct to several packages the Geant4 Outuput

        - Use Geant4 for simulation and data store
        - Save data as root file
        - Control histograms are generated/filled while the program is running
        - Visualization is done using VTK/Paraview toolkit
    """


    def __init__(self, gdmlFileName = None):

        self.histo = {}
        self.adcG4HitCollection = {}
        self.myScore = {}


        """
            Initializes the Geant4 Services
        """

        #TODO: Test if file exists
        self.gdmlFileName = gdmlFileName

        #Prepare a different seed each time it runs
        rndNum = self.getRandomSeed("I")
        print ("======> INFO: Initializing Random Number Generator with seed = ",rndNum)
        self.rand_engine= Geant4.Ranlux64Engine()
        Geant4.HepRandom.setTheEngine(self.rand_engine)
        Geant4.HepRandom.setTheSeed(rndNum)


        #Geometry
        print ("======> INFO: Initializing Geometry using ", self.gdmlFileName)
        self.AdcDC= AdcG4DetectorConstruction.AdcG4DetectorConstruction(self.gdmlFileName)
        Geant4.gRunManager.SetUserInitialization(self.AdcDC)


        #Physics List; For other process, it may need to be done at external C++ code
        print ("======> INFO: Initializing Physics List ")
        self.adcPL = AdcG4Physics.AdcG4PhysicsList()
        Geant4.gRunManager.SetUserInitialization(self.adcPL)


        print ("======> INFO: Initializing Run Action ")
        self.AdcRA= AdcG4RunAction.AdcG4RunAction()
        Geant4.gRunManager.SetUserAction(self.AdcRA)

        print ("======> INFO: Initializing Event User Action ")
        self.adcEA = AdcG4EventAction.AdcG4EventAction()
        self.adcEA.setHitCollectionName(self.adcG4HitCollection)
        Geant4.gRunManager.SetUserAction(self.adcEA)

        print ("======> INFO: Initializing the particle gun ")
        self.adcPg= g4pytest.ParticleGun.Construct()
        #adcPg = Geant4.G4ParticleGun()

        #call the VTK wrapper
        print ("======> INFO: Initializing VTK for visualization")
        self.vtk = AdcG4Vtk.AdcG4Vtk()

        #ROOT tree for output
        print ("======> INFO: Initializing ROOT Services")
        self.AdcTree = AdcG4Root.AdcG4Root()
        self.adcEA.setTree(self.AdcTree)

        Geant4.gRunManager.Initialize()
        self.AdcDC.getLogicalVolumes()
        self.AdcDC.Colorize()

        #Save the RunManager
        self.RunManager = Geant4.gRunManager.GetRunManager()


        #the  default case for Particle Gun

        self.bulet = {}
        self.bulet["Number"] = 1
        self.bulet["Particle"] = "proton"
        self.bulet["Energy"] = 10000
        self.bulet["MomentumDir"] = [0., 0., -1.]
        self.bulet["Position"] = [0., 0., 250.]




    def getRandomSeed(self,tipo = "I"):
        """
            Return a random seed of size "tipo" (number of bytes)
        """
        validTypes = ["h","H","i","I","l","L","q","Q"]
        if tipo not in validTypes :
            print ("======> ERROR: type must be one of these: ", validTypes)
            return -1

        nb = struct.Struct(tipo).size
        print ("======> INFO: Getting Seed from /dev/random :")
        f = open("/dev/random","rb")
        rndStr = f.read(nb)
        rndNum= struct.unpack(tipo, rndStr)[0]

        return rndNum

    def setSensitiveDetector (self):
        """
            Create Sensite Detctor for all volumes by default
        """
        for lv in self.AdcDC.logicalVolumes.keys() :
            sdName = lv + "_SD"
            self.adcG4HitCollection[sdName] = AdcG4HitCollection.AdcG4HitCollection()
            self.myScore[lv] = AdcG4ScoreSensitiveDetector.AdcG4ScoreSensitiveDetector(sdName,self.adcG4HitCollection)
            self.AdcDC.logicalVolumes[lv][0].SetSensitiveDetector(self.myScore[lv])


    def AdcRevolver(self,n = 10):
        """
            Setup a particle gun
        """


        px = self.bulet["MomentumDir"][0]
        py = self.bulet["MomentumDir"][1]
        pz = self.bulet["MomentumDir"][2]

        x = self.bulet["Position"][0]
        y = self.bulet["Position"][1]
        z = self.bulet["Position"][2]

        self.adcPg.SetNumberOfParticles(self.bulet["Number"])
        self.adcPg.SetParticleByName(Geant4.G4String(self.bulet["Particle"]))
        self.adcPg.SetParticleEnergy(self.bulet["Energy"]*Geant4.GeV)
        self.adcPg.SetParticleMomentumDirection(Geant4.G4ThreeVector(px,py,pz))
        self.adcPg.SetParticlePosition(Geant4.G4ThreeVector(x,y,z)*Geant4.cm)


        self.RunManager.BeamOn(n)


    def AdcInitVisualization(self):
        """
            visualization of the gun firing ...
        """
        
        #Geant4.gApplyUICommand(Geant4.G4String("/physics/addPhysics radioactive_decay"))

        Geant4.gApplyUICommand(Geant4.G4String("/vis/open OGLIX"))
        Geant4.gApplyUICommand(Geant4.G4String("/vis/scene/create"))
        Geant4.gApplyUICommand(Geant4.G4String("/vis/scene/add/volume"))
        Geant4.gApplyUICommand(Geant4.G4String("/vis/scene/add/axes"))
        Geant4.gApplyUICommand(Geant4.G4String("/vis/scene/add/hits"))
        Geant4.gApplyUICommand(Geant4.G4String("/vis/scene/add/eventId"))
        Geant4.gApplyUICommand(Geant4.G4String("/vis/sceneHandler/attach"))
        Geant4.gApplyUICommand(Geant4.G4String("/vis/viewer/set/viewpointThetaPhi 120. 60."))
        Geant4.gApplyUICommand(Geant4.G4String("/vis/viewer/set/style solid"))
        #Geant4.gApplyUICommand("/vis/viewer/set/style wired")
        Geant4.gApplyUICommand("/vis/viewer/set/viewpointThetaPhi 90. 0.")

        Geant4.gApplyUICommand(Geant4.G4String("/tracking/storeTrajectory 1"))
        Geant4.gApplyUICommand(Geant4.G4String("/vis/scene/add/trajectories smooth rich"))

        Geant4.gApplyUICommand(Geant4.G4String("/vis/scene/endOfEventAction accumulate"))

        Geant4.gApplyUICommand("/vis/viewer/zoomTo 0.5")

        

